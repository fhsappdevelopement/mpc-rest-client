﻿using System;
using System.Text;

namespace PCM.MPC.WCFRest
{
    public class Base64Converter
    {
        public static string toUrlBase64(string toConvert)
        {
            return toUrlBase64(Encoding.UTF8.GetBytes(toConvert));
        }

        public static string toUrlBase64(byte[] bytes)
        {
            return Convert.ToBase64String(bytes).Replace('/', '_').Replace('+', '-');
        }

        public static byte[] fromUrlBase64(string base64String)
        {
            return Convert.FromBase64String(base64String.Replace('+', '-').Replace('/', '_'));
        }

    }
}
