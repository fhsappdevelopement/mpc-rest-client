﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfLieferant", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfDelivery
    {
        [XmlElement("Lieferant")]
        public Lieferant[] list { get; set; }
    }
}
