﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfEkl", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfEkl
    {
        [XmlElement("Ekl")]
        public Ekl[] list { get; set; }
    }
}
