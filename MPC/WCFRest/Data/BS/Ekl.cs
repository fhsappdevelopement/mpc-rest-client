﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("Ekl")]
    public class Ekl
    {
        [XmlElement("EklID")]
        public int ID { get; set; }

        [XmlElement("EklNr")]
        public string EklNr { get; set; }

        [XmlElement("LID")]
        public int LID { get; set; }

        [XmlElement("LiefBez1")]
        public string LiefBez1 { get; set; }

        [XmlElement("Bez")]
        public string Bez { get; set; }

        [XmlElement("Bem")]
        public string Bem { get; set; }

        [XmlElement("AnzPos")]
        public int AnzPos { get; set; }
    }
}
