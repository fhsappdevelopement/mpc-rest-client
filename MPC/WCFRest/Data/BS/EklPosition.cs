﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("EklPosition")]
    public class EklPosition
    {
        [XmlElement("EklID")]
        public int EklID { get; set; }

        [XmlElement("EklPosNr")]
        public int EklPosNr { get; set; }

        [XmlElement("BEHMenge")]
        public double BEHMenge { get; set; }

        [XmlElement("BEHBez")]
        public string BEHBez { get; set; }

        [XmlElement("Article")]
        public LIDArticle Article { get; set; }
    }
}
