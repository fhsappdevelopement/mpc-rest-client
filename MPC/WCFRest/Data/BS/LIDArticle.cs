﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("LIDArticle")]
    public class LIDArticle
    {
        [XmlElement("LID")]
        public int LID { get; set; }

        [XmlElement("ArtID")]
        public int ArtID { get; set; }

        [XmlElement("PID")]
        public int PID { get; set; }

        [XmlElement("ArtNr")]
        public string ArtNr { get; set; }

        [XmlElement("BezPCM")]
        public string BezPCM { get; set; }

        [XmlElement("ILN")]
        public string ILN { get; set; }

        [XmlElement("LiefBez")]
        public string LiefBez { get; set; }
    }
}
