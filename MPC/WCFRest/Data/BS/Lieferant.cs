﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("Lieferant")]
    public class Lieferant
    {
        [XmlElement("LID")]
        public int ID { get; set; }

        [XmlElement("LiefBez1")]
        public string Description { get; set; }

        [XmlElement("LiefStraße")]
        public string LiefStrasse { get; set; }

        [XmlElement("LiefPLZ")]
        public string LiefPLZ { get; set; }

        [XmlElement("LiefOrt")]
        public string LiefOrt { get; set; }
    }
}
