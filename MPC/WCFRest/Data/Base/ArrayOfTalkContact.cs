﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    [XmlRoot(ElementName = "ArrayOfTalkContact", Namespace= "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfTalkContact
    {
        [XmlElement("TalkContact")]
        public TalkContact[] list { get; set; }
    }
}
