﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    [XmlRoot(ElementName ="ArrayOfTalkMessage", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfTalkMessage
    {
        [XmlElement("TalkMessage")]
        public TalkMessage[] list { get; set; }
    }
}
