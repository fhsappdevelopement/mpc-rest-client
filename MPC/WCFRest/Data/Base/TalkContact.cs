﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    public class TalkContact
    {
        [XmlElement("Nachname")]
        public string Nachname { get; set; }

        [XmlElement("Vorname")]
        public string Vorname { get; set; }

        [XmlElement("Abteilung")]
        public string Abteilung { get; set; }

        [XmlElement("PSID")]
        public int PSID { get; set; }

        // Status der Person; 0 = abgemeldet
        // 1 = frei
        // 2 = besetzt
        // 3 = abgemeldet
        // 4 = Termin
        // 5 = nicht stören
        [XmlElement("StatusID")]
        public int StatusID { get; set; }
    }
}
