﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    [XmlRoot("ArrayOfFileName")]
    public class ArrayOfFileName
    {
        // Must be XmlElement, to prevent the wrapping "list".
        // See: http://stackoverflow.com/questions/2006482/xml-serialization-disable-rendering-root-element-of-array
        // See: D_370_MPCRest_Client.pdf 3.3.2
        [XmlElement("FileName")]
        public string[] list { get; set; }
    }
}
