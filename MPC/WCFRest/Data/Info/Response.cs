﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    /// <summary>
    /// MPC Webservice Protocol class. See the PCM documentation
    /// for more information.
    /// </summary>
    [XmlRoot("Response")]
    public class Response
    {
        [XmlElement("ErrorText")]
        public string ErrorText { get; set; }

        [XmlElement("Result")]
        public string Result { get; set; }
    }
}
