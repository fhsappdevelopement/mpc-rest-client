﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfMnu", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfMnu
    {
        [XmlElement("Mnu")]
        public Mnu[] list { get; set; }
    }
}
