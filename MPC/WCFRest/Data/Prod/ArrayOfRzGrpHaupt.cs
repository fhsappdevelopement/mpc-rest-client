﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfRzGrpHaupt", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfRzGrpHaupt
    {
        [XmlElement("RzGrpHaupt")]
        public RzGrpHaupt[] list { get; set; }
    }
}
