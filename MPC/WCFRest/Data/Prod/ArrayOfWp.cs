﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfWp", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfWp
    {
        [XmlElement("Wp")]
        public Wp[] list { get; set; }
    }
}
