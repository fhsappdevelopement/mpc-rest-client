﻿using System;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("ArrayOfWpPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class ArrayOfWpPos
    {
        [XmlElement("WpPos")]
        public WpPos[] list { get; set; }
    }
}
