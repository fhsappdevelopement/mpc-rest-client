﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("Mnu", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class Mnu
    {
        [XmlElement("MnuID")]
        // ArtID des Menüs
        public int MnuID { get; set; }
        
        [XmlElement("MID")]
        // ArtID des Mandanten
        public int MID { get; set; }
        
        [XmlElement("Bez")]
        // Bezeichnung des Menüs
        public string Bez { get; set; }
        
        [XmlElement("PreisVK")]
        // Verkaufspreis des Menüs
        public double PreisVK { get; set; }
        
        [XmlArray("MnuPosList"), XmlArrayItem("MnuPos", typeof(MnuPos))]
        // Menü-Positionen
        public MnuPos[] MnuPosList { get; set; }


    }
}