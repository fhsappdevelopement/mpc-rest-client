﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("RzPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class Rz
    {
        [XmlElement("RzID")]
        // ID des Rezepts
        public int RzID { get; set; }

        [XmlElement("RzGrpID")]
        // Verweis auf die ID der RezeptGruppe
        public int RzGrpID { get; set; }

        [XmlElement("MID")]
        // ID des Mandanten
        public int MID { get; set; }

        [XmlElement("Bez")]
        // Bezeichnung des Rezepts
        public string Bez { get; set; }

        [XmlElement("Preis")]
        // Preis des Rezepts
        public double Preis { get; set; }

        [XmlElement("Bearbeiter")]
        // Name des Mitarbeiters der das Rezept zuletzt bearbeitet hat
        public string Bearbeiter { get; set; }

        [XmlElement("LastModDat")]
        // Datum der letzten Rezeptbearbeitung
        public string LastModDat { get; set; }

        [XmlArray("RzPosList"), XmlArrayItem("RzPos", typeof(RzPos))]
        // Rezept-Positionen
        public RzPos[] RzPosList { get; set; }
    }
}
