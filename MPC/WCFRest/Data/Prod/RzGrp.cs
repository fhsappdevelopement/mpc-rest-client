﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("RzGrp")]
    public class RzGrp
    {
        [XmlElement("RzGrpID")]
        public int RzGrpID { get; set; }

        [XmlElement("RzGrpHauptID")]
        public int RzGrpHauptID { get; set; }

        [XmlElement("MID")]
        public int MID { get; set; }

        [XmlElement("Bem")]
        public string Bem { get; set; }

        [XmlElement("Bez")]
        public string Bez { get; set; }
    }
}
