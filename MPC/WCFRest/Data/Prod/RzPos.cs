﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("RzPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class RzPos
    {
        [XmlElement("RzPosID")]
        // ID der Rezeptposition
        public int RzPosID { get; set; }

        [XmlElement("ArtID")]
        // Verweis auf die ID des Artikels
        public int ArtID { get; set; }

        [XmlElement("MengeEinh")]
        // Mengeneinheit Kürzel
        public string MengeEinh { get; set; }

        [XmlElement("LID")]
        // ID des Lieferanten, der den Artikel des Rezepts liefert
        public int LID { get; set; }

        [XmlElement("MID")]
        // ID des Mandanten
        public int MID { get; set; }

        [XmlElement("RzID")]
        // Verweis auf das Rezept zu dem die Position gehört
        public int RzID { get; set; }

        [XmlElement("sortID")]
        // Sortier Reihenfolge, für die Darstellung der Positionen
        public int SortID { get; set; }

        [XmlElement("Bem")]
        // Bemerkung zum Rezept
        public string Bem { get; set; }

        [XmlElement("PosBez")]
        // Bezeichnung der Rezeptposition
        public string PosBez { get; set; }

        [XmlElement("Menge")]
        // Menge des Artikels, der in der Position Verwendung findet
        public double Menge { get; set; }

        [XmlElement("PreisProME")]
        // der Preis pro Mengeneinheit
        public double PreisProME { get; set; }
    }
}
