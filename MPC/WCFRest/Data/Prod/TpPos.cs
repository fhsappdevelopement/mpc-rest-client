﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("TpPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class TpPos
    {
        [XmlElement("TpPosID")]
        // ArtID der Tagesplanposition
        public int TpPosID { get; set; }

        [XmlElement("TpID")]
        // Verweis auf den Tagesplan zu dem die Position gehört
        public int TpID { get; set; }

        [XmlElement("ArtID")]
        // Verweis auf die ArtID des Artikels, wenn die Position
        public int ArtID { get; set; }

        [XmlElement("LID")]
        // ArtID des Lieferanten, der den Artikel des Tagesplans liefert
        public int LID { get; set; }

        [XmlElement("BezSp")]
        // Bezeichnung des Tagesplans im Speiseplan
        public string BezSp { get; set; }

        [XmlElement("RzID")]
        //ArtID des Rezeptes, falls die Position ein Rezept ist
        public int RzID { get; set; }

        [XmlElement("MnuID")]
        //ArtID des Menüs, falls die Position ein Menü ist
        public int MnuID { get; set; }

        [XmlElement("PosArt")]
        // Positionsart 1 - Artikel 2 - Rezept
        public int PosArt { get; set; }

        [XmlElement("Anzahl")]
        // Anzahl der in der Position aufgeführten Artefakte
        public int Anzahl { get; set; }

    }
}