﻿using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Data
{
    [XmlRoot("WpPos", Namespace = "http://schemas.datacontract.org/2004/07/PCM.MPCSvc.Generic")]
    public class WpPos
    {
        [XmlElement("WpPosID")]
        // ArtID der Wochenplanposition
        public int WpPosID { get; set; }

        [XmlElement("WpID")]
        // Verweis auf den Wochenplan zu dem die Position gehört
        public int WpID { get; set; }

        [XmlElement("Tag")]
        // Tag in der Woche zu dem die Wochenplanposition gehört
        public int Tag { get; set; }

        [XmlElement("TpID")]
        // ArtID des Tagesplans, der dem Wochenplan zugeordnet wird
        public int TpID { get; set; }

        [XmlElement("Woche")]
        // Woche zu dem die Wochenplanposition gehört
        public int Woche { get; set; }
    }
}