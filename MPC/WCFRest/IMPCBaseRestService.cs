﻿using PCM.MPC.WCFRest.Data;
using System.Threading.Tasks;

namespace PCM.MPC.WCFRest
{
    public interface IMPCBaseRestService
    {
        /// <summary>
        /// The /TalkContact/GetItems resource endpoint. Gets the list of available contacts.
        /// </summary>
        /// <returns></returns>
        Task<ArrayOfTalkContact> TalkContactGetItems();

        /// <summary>
        /// The /TalkMessage/{MID}/AddItems/{SESSIONGUID} resource endpoint. Sends new messaged to the webservice.
        /// </summary>
        /// <param name="messages"></param>
        /// <param name="mid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<Response> TalkMessageAddItems(ArrayOfTalkMessage messages, int mid, string sessionGUID);

        /// <summary>
        /// The /TalkMessage/{MID}/GetItems/{SESSIONGUID}/{DSIDLimit}/{Count} resource endpoint. Receives new messages.
        /// </summary>
        /// <param name="mid"></param>
        /// <param name="sessionGUID"></param>
        /// <returns></returns>
        Task<ArrayOfTalkMessage> TalkMessageGetItems(int mid, string sessionGUID);
    }
}
