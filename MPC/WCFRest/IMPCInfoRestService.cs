﻿using PCM.MPC.WCFRest.Data;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.WCFRest
{
    public interface IMPCInfoRestService
    {
        /// <summary>
        /// The /Info/{MID}/GetItem/{UserName}/{Password} resource endpoint.
        /// </summary>
        /// <remarks>Authentication against the PCM rest webservice. The authentication 
        /// was successfully if the InfoResponse instance has a valid
        /// session guid.</remarks>
        /// <param name="credentials">Credentials.</param>
        /// <returns>InfoResponse.</returns>
        /// <see cref="MPCCredentials"/>
        /// <seealso cref="InfoResponse"/>
        Task<InfoResponse> InfoGetItem(MPCCredentials credentials);

        /// <summary>
        /// The /InfoImage/GetList resource endpoint. List of contact images filenames.
        /// </summary>
        /// <returns>ArrayOfFileName</returns>
        /// <seealso cref="ArrayOfFileName"/>
        Task<ArrayOfFileName> InfoImageGetList();

        /// <summary>
        /// The /InfoImage/GetItem/{MpcPsID.png} resource endpoint.Concrete image of a contact.
        /// </summary>
        /// <param name="filename">The MPC PS ArtID.</param>
        /// <returns>BitmapImage.</returns>
        /// <seealso cref="BitmapImage"/>
        Task<BitmapImage> InfoImageGetItem(string filename);
    }
}
