﻿using System;
using System.Text;

namespace PCM.MPC.WCFRest
{
    /// <summary>
    /// Store the credentials for authentication purposes.
    /// </summary>
    public class MPCCredentials
    {
        /// <summary>
        /// MandatenID property.
        /// </summary>
        public int MandantenID { get; set; }

        /// <summary>
        /// Username property.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Password property.
        /// </summary>
        /// <remarks>Encoded in base64 for submitting purposes. This 
        /// is not an encryption!</remarks>
        public string Password { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="username">Username.</param>
        /// <param name="password">Password in base64.</param>
        /// <param name="mandantenID">Mandaten ID.</param>
        public MPCCredentials(string username, string password, int mandantenID)
        {
            this.Username = username;
            this.Password = password;
            this.MandantenID = mandantenID;
        }
    }
}
