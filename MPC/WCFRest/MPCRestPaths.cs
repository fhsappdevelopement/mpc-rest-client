﻿namespace PCM.MPC.WCFRest
{
    class MPCRestPaths
    {
        public class Info
        {
            /// <summary>
            /// "/InfoImage/GetList"
            /// </summary>
            public static string InfoImageGetList
            {
                get
                {
                    return "/InfoImage/GetList";
                }
            }

            /// <summary>
            /// "/Info/{0}/GetItem/{1}/{2}"
            /// 0: MID, 1: UserName, 2: Password
            /// </summary>
            public static string InfoGetItem
            {
                get
                {
                    return "/Info/{0}/GetItem/{1}/{2}/MPCClient";
                }
            }

            /// <summary>
            /// "/InfoImage/GetItem/{0}"
            /// 0: filename
            /// NOTE: it would be better if here only stands the psid, but the webservice
            /// expects a filename and the extension of the file is not unique
            /// </summary>
            public static string InfoImageGetItem
            {
                get
                {
                    return "/InfoImage/GetItem/{0}";
                }
            }

            /// <summary>
            /// /SysImage/GetList
            /// </summary>
            public static string SysImageGetList
            {
                get
                {
                    return "/SysImage/GetList";
                }
            }

            /// <summary>
            /// /SysImage/GetItem/{0}
            /// 0: FileName mit extension
            /// </summary>
            public static string SysImageGetItem
            {
                get
                {
                    return "/SysImage/GetItem/{0}";
                }
            }
        }


        public class Base
        {
            /// <summary>
            /// "/TalkContact/GetItems"
            /// </summary>
            public static string TalkContactGetItems
            {
                get
                {
                    return "/TalkContact/GetItems";
                }
            }

            /// <summary>
            /// POST and as Stream
            /// /TalkMessage/{0}/AddItems/{1}
            /// 
            /// 0: MID
            /// 1: SESSIONGUID
            /// </summary>
            public static string TalkMessageAddItems
            {
                get
                {
                    return "/TalkMessage/{0}/AddItems/{1}";
                }
            }

            /// <summary>
            /// /TalkMessage/{0}/GetItems/{1}/{2}/{3}
            /// 
            /// 0: MID
            /// 1: SESSIONGUID
            /// 2: DSIDLimit
            /// 3: Count
            /// </summary>
            public static string TalkMessageGetItems
            {
                get
                {
                    return "/TalkMessage/{0}/GetItems/{1}/{2}/{3}";
                }
            }
        }

        public class BS
        {
            /// <summary>
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string EklLiferantGetItems
            {
                get
                {
                    return "/EklLieferant/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string EklGetItems
            {
                get
                {
                    return "/Ekl/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// 0: MID, 1: EKLID, 2: SESSIONGUID
            /// </summary>
            public static string EklPositionGetItems
            {
                get
                {
                    return "/EklPosition/{0}/GetItems/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string EklPositionUpdateItems
            {
                get
                {
                    return "/EklPosition/{0}/UpdateItem/{1}";
                }
            }

            /// <summary>
            /// 0: MID, 1: ARTNR, 2: SESSIONGUID
            /// </summary>
            public static string LIDArticleGetItemByArtNr
            {
                get
                {
                    return "/LIDArticle/{0}/GetItembyArtNr/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: LID, 2: SESSIONGUID
            /// </summary>
            public static string LIDArticleGetItemByLID
            {
                get
                {
                    return "/LIDArticle/{0}/GetItembyLID/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: PCM Bez Article, 2: SESSIONGUID
            /// </summary>
            public static string LIDArticleGetItemByBezPCM
            {
                get
                {
                    return "/LIDArticle/{0}/GetItembyBezPCM/{1}/{2}";
                }
            }
        }

        public class Prod
        {
            /// <summary>
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string RzHierarchieGetItems
            {
                get
                {
                    return "/RzHierarchie/{0}/GetItems/{1}";
                }
            }


            /// <summary>
            /// "Die Rezepte mit Rezeptpositionen des Mandanten werden abgefragt. "
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string RzGetItems
            {
                get
                {
                    return "/Rz/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// 0: MID, 1:rezept id, 2: SESSIONGUID
            /// </summary>
            public static string RzGetItemByID
            {
                get
                {
                    return "/Rz/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1:rezept id, 2: SESSIONGUID
            /// </summary>
            public static string RzPosGetItemByID
            {
                get
                {
                    return "/RzPos/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1:CRUD operation, 2: SESSIONGUID
            /// </summary>
            public static string RzPosCRUD
            {
                get
                {
                    return "/RzPos/{0}/CRUD/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: SESSIONGUID
            /// </summary>
            public static string MnuGetItems
            {
                get
                {
                    return "/Mnu/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// 0: MID, 1: MNUID 2: SESSIONGUID
            /// </summary>
            public static string MnuGetItemByID
            {
                get
                {
                    return "/Mnu/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: MNUID 2: SESSIONGUID
            /// </summary>
            public static string MnuPosGetItemByID
            {
                get
                {
                    return "/MnuPos/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// 0: MID, 1: Crud Method C R U D 2: SESSIONGUID
            /// </summary>
            public static string MnuPosCRUD
            {
                get
                {
                    return "/MnuPos/{0}/CRUD/{1}/{2}";
                }
            }

            /// <summary>
            /// Alle Tagespläne mit Tagesplanpositionen des Mandanten werden abgerufen.
            /// 0: MID, 1 SESSIONGUID
            /// </summary>
            public static string TpGetItems
            {
                get
                {
                    return "/Tp/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// Ein bestimmter Tagesplan mit Tagesplanpositionen des Mandanten wird abgerufen
            /// 0: MID, 1: Tagesplan-ArtID des Tagesplanes, das abgerufen werden soll, 2: SESSIONGUID
            /// </summary>
            public static string TpGetItemByID
            {
                get
                {
                    return "/Tp/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// Alle Tagesplanpositionen zu einem bestimmten Tagesplan des Mandanten werden abgefragt.
            /// 0: MID, 1: Tagesplan-ArtID des Tagesplanes, das abgerufen werden soll, 2: SESSIONGUID
            /// </summary>
            public static string TpPosGetItemByID
            {
                get
                {
                    return "TpPos/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// Create, Read, Update oder Delete einer Tagesplanposition
            /// 0: MID, 1:CRUD operation, 2: SESSIONGUID
            /// </summary>
            public static string TpPosCRUD
            {
                get
                {
                    return "TpPos/{0}/CRUD/{1}/{2}";
                }
            }

            /// <summary>
            /// Alle Wochenpläne mit Wochenplanpositionen des Mandanten werden abgerufen.
            /// 0: MID, 1 SESSIONGUID
            /// </summary>
            public static string WpGetItems
            {
                get
                {
                    return "Wp/{0}/GetItems/{1}";
                }
            }

            /// <summary>
            /// Ein bestimmter Wochenplan mit Wochenplanpositionen des Mandanten wird abgerufen.
            /// 0: MID, 1: WPID = Wochenplan-ArtID des Wochenplanes, das abgerufen werden soll, 2: SESSIONGUID
            /// </summary>
            public static string WpGetItemById
            {
                get
                {
                    return "Wp/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// Alle Wochenplanpositionen zu einem bestimmten Wochenplan des Mandanten werden abgefragt.
            /// 0: MID, 1: WPID = Wochenplan-ArtID des Wochenplanes, das abgerufen werden soll, 2: SESSIONGUID
            /// </summary>
            public static string WpPosGetItemById
            {
                get
                {
                    return "WpPos/{0}/GetItembyID/{1}/{2}";
                }
            }

            /// <summary>
            /// Create, Read, Update oder Delete einer Wochenplanposition
            /// 0: MID, 1:CRUD operation, 2: SESSIONGUID
            /// </summary>
            public static string WpPosCRUD
            {
                get
                {
                    return "WpPos/{0}/CRUD/{1}/{2}";
                }
            }
        }
    }

}
