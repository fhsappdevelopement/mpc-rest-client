﻿using System;
using System.Threading.Tasks;
using PCM.MPC.WCFRest.Data;
using PCM.REST.Client;
using PCM.MPC.WCFRest.Serializer;
using System.Net.Http;
using System.IO;
using Windows.UI.Xaml.Media.Imaging;

namespace PCM.MPC.WCFRest
{
    /// <summary>
    /// Implementation of the IMPCRestService interface.
    /// </summary>
    /// <seealso cref="IMPCInfoRestService"/>
    public class MPCRestService : IMPCRestService
    {
        /// <summary>
        /// Store for the RestClient property.
        /// </summary>
        protected RestClient restClient;

        /// <summary>
        /// Store for the Serializer property.
        /// </summary>
        protected MPCXmlSerializer serializer;

        /// <summary>
        /// RestClient property.
        /// </summary>
        public RestClient RestClient
        {
            get
            {
                return this.restClient;
            }
        }

        /// <summary>
        /// Serializer property.
        /// </summary>
        public MPCXmlSerializer Serializer
        {
            get
            {
                return this.serializer;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="restClient">Rest client.</param>
        /// <param name="serializer">Serializer.</param>
        public MPCRestService(RestClient restClient, MPCXmlSerializer serializer)
        {
            this.restClient = restClient;
            this.serializer = serializer;
        }

        public async Task<InfoResponse> InfoGetItem(MPCCredentials credentials)
        {
            var parameters = new[] {
                credentials.MandantenID.ToString(),
                credentials.Username,
                credentials.Password
            };

            HttpResponseMessage responseMessage = await this.restClient.Get(MPCRestPaths.Info.InfoGetItem, parameters);
            MPCDeserializerResult<InfoResponse> result = await this.deserialize<InfoResponse>(responseMessage);

            return result.Result;
        }

        /// <summary>
        /// Delegate method to the Serializer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="responseMessage"></param>
        /// <returns></returns>
        protected async Task<MPCDeserializerResult<T>> deserialize<T>(HttpResponseMessage responseMessage) where T : class
        {
            MPCDeserializerResult<T> result = this.serializer.deserialize<T>(await responseMessage.Content.ReadAsStringAsync());

            if (result.IsResponse() && typeof(T) != typeof(Response) &&
                result.Response.ErrorText != null && result.Response.ErrorText.Length > 0)
            {
                throw new MPCRestServiceException(result.Response.ErrorText);
            }

            return result;
        }

        /// <summary>
        /// Delegate method to the serializer.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toSerialize"></param>
        /// <returns></returns>
        protected string serialize<T>(T toSerialize) where T : class
        {
            string result = this.serializer.serialize<T>(toSerialize);

            if (result == null)
            {
                throw new MPCRestServiceException("Can't serialize " + typeof(T).ToString());
            }

            return result;
        }

        protected async Task<T> doGetRequestAndFetchResult<T>(string path) where T : class
        {
            return await doGetRequestAndFetchResult<T>(path, new string[] { });
        }

        protected async Task<T> doGetRequestAndFetchResult<T>(string path, string[] parameters) where T : class
        {
            HttpResponseMessage responseMessage = await this.restClient.Get(path, parameters);
            MPCDeserializerResult<T> result = await this.deserialize<T>(responseMessage);

            return result.Result;
        }

        protected async Task<T> doPostRequestAndFetchResult<T>(string path, string[] parameters, T objectToPost)
            where T : class
        {
            string serializedObject = this.serialize<T>(objectToPost);
            HttpResponseMessage responseMessage = await this.restClient.PostXml(path, parameters, serializedObject);

            MPCDeserializerResult<T> response = await this.deserialize<T>(responseMessage);

            return response.Result;
        }

        protected async Task<T> doPostRequestAndFetchResult<T, V>(string path, string[] parameters, V objectToPost) 
            where T : class
            where V : class
        {
            string serializedObject = this.serialize<V>(objectToPost);
            HttpResponseMessage responseMessage = await this.restClient.PostXml(path, parameters, serializedObject);

            MPCDeserializerResult<T> response = await this.deserialize<T>(responseMessage);

            return response.Result;
        }

        public async Task<ArrayOfFileName> InfoImageGetList()
        {/*
            HttpResponseMessage responseMessage = await this.restClient.Get(MPCRestPaths.Info.InfoImageGetList);
            MPCDeserializerResult<ArrayOfFileName> result = await this.deserialize<ArrayOfFileName>(responseMessage);

            return result.Result;*/

            return await this.doGetRequestAndFetchResult<ArrayOfFileName>(MPCRestPaths.Info.InfoImageGetList);
        }

        public async Task<BitmapImage> InfoImageGetItem(string filename)
        {
            Stream responseStream = await this.restClient.GetAsStream(MPCRestPaths.Info.InfoImageGetItem, filename);

            // see
            // http://stackoverflow.com/questions/7669311/is-there-a-way-to-convert-a-system-io-stream-to-a-windows-storage-streams-irando
            MemoryStream memStream = new MemoryStream();
            await responseStream.CopyToAsync(memStream);
            memStream.Position = 0;

            BitmapImage image = new BitmapImage();
            image.SetSource(memStream.AsRandomAccessStream());

            return image;
        }

        public async Task<ArrayOfTalkContact> TalkContactGetItems()
        {/*
            HttpResponseMessage responseMessage = await this.restClient.Get(MPCRestPaths.Base.TalkContactGetItems);
            MPCDeserializerResult<ArrayOfTalkContact> result = await this.deserialize<ArrayOfTalkContact>(responseMessage);

            return result.Result;*/
            return await this.doGetRequestAndFetchResult<ArrayOfTalkContact>(MPCRestPaths.Base.TalkContactGetItems);
        }

        public async Task<Response> TalkMessageAddItems(ArrayOfTalkMessage messages, int mid, string sessionGUID)
        {
            var parameters = new[]
            {
                mid.ToString(),
                sessionGUID
            };

            return await doPostRequestAndFetchResult<Response, ArrayOfTalkMessage>(MPCRestPaths.Base.TalkMessageAddItems, parameters, messages);
        }

        public async Task<ArrayOfTalkMessage> TalkMessageGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID,
                "0", "0" // TODO: important? Count and DSID Limit
            };

            HttpResponseMessage responseMessage = await this.restClient.Get(MPCRestPaths.Base.TalkMessageGetItems, parameters);

            // the deserialize method throws an exception if it detects an errortext in the response object,
            // but the webservice returns an error if there no new messages available. best practice would be
            // to return an empty list or - by the choosen design - to put the text "No Messages available -
            // in the Result field. The code below tries to fix this circumstances */
            try
            {
                MPCDeserializerResult<ArrayOfTalkMessage> result = await this.deserialize<ArrayOfTalkMessage>(responseMessage);
                return result.Result;
            }
            catch (MPCRestServiceException)
            {
                // srsly why ... why it's an error if there are no messages available? 
                // this should be fixed on webservice side.
                var arrayOfTalkMessagesFake = new ArrayOfTalkMessage();
                arrayOfTalkMessagesFake.list = new TalkMessage[] { };

                return arrayOfTalkMessagesFake;
            }

        }

        public async Task<ArrayOfDelivery> EklDeliveryGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfDelivery>(MPCRestPaths.BS.EklLiferantGetItems, parameters);
        }

        public async Task<ArrayOfEkl> EklGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfEkl>(MPCRestPaths.BS.EklGetItems, parameters);
        }

        public async Task<ArrayOfEklPosition> EklPositionGetItems(int mid, int eklid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                eklid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfEklPosition>(MPCRestPaths.BS.EklPositionGetItems, parameters);
        }

        public async Task<Response> EklPositionUpdateItems(int mid, string sessionGUID, ArrayOfEklPosition updateItems)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            string serializedMessages = this.serialize<ArrayOfEklPosition>(updateItems);
            HttpResponseMessage responseMessage = await this.restClient.PostXml(MPCRestPaths.BS.EklPositionUpdateItems, parameters, serializedMessages);

            MPCDeserializerResult<Response> response = await this.deserialize<Response>(responseMessage);

            // TODO: check for error text and throw exception? 

            return response.Result;
        }

        public async Task<ArrayOfLIDArticle> LIDArticleGetItemByArticleNumber(int mid, int articleNumber, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                articleNumber.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfLIDArticle>(MPCRestPaths.BS.LIDArticleGetItemByArtNr, parameters);
        }

        public async Task<ArrayOfLIDArticle> LIDArticleGetItemByLId(int mid, int lid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                lid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfLIDArticle>(MPCRestPaths.BS.LIDArticleGetItemByLID, parameters);
        }

        public async Task<ArrayOfLIDArticle> LIDArticleGetItemByDescription(int mid, string description, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                description,
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfLIDArticle>(MPCRestPaths.BS.LIDArticleGetItemByBezPCM, parameters);
        }

        public async Task<ArrayOfRzGrpHaupt> RzHierarchieGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfRzGrpHaupt>(MPCRestPaths.Prod.RzHierarchieGetItems, parameters);
        }

        public async Task<ArrayOfRz> RzGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfRz>(MPCRestPaths.Prod.RzGetItems, parameters);
        }

        public async Task<Rz> RzGetItemById(int mid, int rzid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                rzid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<Rz>(MPCRestPaths.Prod.RzGetItemByID, parameters);
        }

        public async Task<ArrayOfRzPos> RzPosGetItemById(int mid, int rzid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                rzid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfRzPos>(MPCRestPaths.Prod.RzPosGetItemByID, parameters);
        }

        public async Task<RzPos> RzPosCreate(RzPos rzPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "C",
                sessionGUID
            };
            return await this.doPostRequestAndFetchResult<RzPos>(MPCRestPaths.Prod.RzPosCRUD, parameters, rzPos);
            
        }

        public async Task<RzPos> RzPosRead(RzPos rzPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "R",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<RzPos>(MPCRestPaths.Prod.RzPosCRUD, parameters, rzPos);
        }

        public async Task<RzPos> RzPosUpdate(RzPos rzPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "U",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<RzPos>(MPCRestPaths.Prod.RzPosCRUD, parameters, rzPos);
        }

        public async Task<RzPos> RzPosDelete(RzPos rzPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "D",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<RzPos>(MPCRestPaths.Prod.RzPosCRUD, parameters, rzPos);
        }

        public async Task<ArrayOfMnu> MnuGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfMnu>(MPCRestPaths.Prod.MnuGetItems, parameters);
        }

        public async Task<ArrayOfMnu> MnuGetItemById(int mid, int mnuId, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                mnuId.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfMnu>(MPCRestPaths.Prod.MnuGetItemByID, parameters);
        }

        public async Task<ArrayOfMnuPos> MnuPosGetItemById(int mid, int mnuID, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                mnuID.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfMnuPos>(MPCRestPaths.Prod.MnuPosGetItemByID, parameters);
        }

        public async Task<MnuPos> MnuPosCreate(MnuPos mnuPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "C",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<MnuPos>(MPCRestPaths.Prod.MnuPosCRUD, parameters, mnuPos);
        }

        public async Task<MnuPos> MnuPosRead(MnuPos mnuPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "R",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<MnuPos>(MPCRestPaths.Prod.MnuPosCRUD, parameters, mnuPos);
        }

        public async Task<MnuPos> MnuPosUpdate(MnuPos mnuPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "U",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<MnuPos>(MPCRestPaths.Prod.MnuPosCRUD, parameters, mnuPos);
        }

        public async Task<MnuPos> MnuPosDelete(MnuPos mnuPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "D",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<MnuPos>(MPCRestPaths.Prod.MnuPosCRUD, parameters, mnuPos);
        }

        public async Task<ArrayOfTp> TpGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfTp>(MPCRestPaths.Prod.TpGetItems, parameters);
        }

        public async Task<ArrayOfTp> TpGetItemById(int mid, int tpid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                tpid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfTp>(MPCRestPaths.Prod.TpGetItemByID, parameters);
        }

        public async Task<ArrayOfTpPos> TpPosGetItemById(int mid, int tpid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                tpid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfTpPos>(MPCRestPaths.Prod.TpPosGetItemByID, parameters);
        }

        public async Task<TpPos> TpPosCreate(TpPos tpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "C",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<TpPos>(MPCRestPaths.Prod.TpPosCRUD, parameters, tpPos);
        }

        public async Task<TpPos> TpPosRead(TpPos tpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "R",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<TpPos>(MPCRestPaths.Prod.TpPosCRUD, parameters, tpPos);
        }

        public async Task<TpPos> TpPosUpdate(TpPos tpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "U",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<TpPos>(MPCRestPaths.Prod.TpPosCRUD, parameters, tpPos);
        }

        public async Task<TpPos> TpPosDelete(TpPos tpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "D",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<TpPos>(MPCRestPaths.Prod.TpPosCRUD, parameters, tpPos);
        }

        public async Task<ArrayOfWp> WpGetItems(int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfWp>(MPCRestPaths.Prod.WpGetItems, parameters);
        }

        public async Task<ArrayOfWp> WpGetItemById(int mid, int wpid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                wpid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfWp>(MPCRestPaths.Prod.WpGetItemById, parameters);
        }

        public async Task<ArrayOfWpPos> WpPosGetItemById(int mid, int wpid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                wpid.ToString(),
                sessionGUID
            };

            return await this.doGetRequestAndFetchResult<ArrayOfWpPos>(MPCRestPaths.Prod.WpPosGetItemById, parameters);
        }

        public async Task<WpPos> WpPosCreate(WpPos wpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "C",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<WpPos>(MPCRestPaths.Prod.WpPosCRUD, parameters, wpPos);
        }

        public async Task<WpPos> WpPosRead(WpPos wpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "R",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<WpPos>(MPCRestPaths.Prod.WpPosCRUD, parameters, wpPos);
        }

        public async Task<WpPos> WpPosUpdate(WpPos wpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "U",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<WpPos>(MPCRestPaths.Prod.WpPosCRUD, parameters, wpPos);
        }

        public async Task<WpPos> WpPosDelete(WpPos wpPos, int mid, string sessionGUID)
        {
            var parameters = new[] {
                mid.ToString(),
                "D",
                sessionGUID
            };

            return await this.doPostRequestAndFetchResult<WpPos>(MPCRestPaths.Prod.WpPosCRUD, parameters, wpPos);
        }
    }
}
