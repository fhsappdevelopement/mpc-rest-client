﻿using System;

namespace PCM.MPC.WCFRest
{
    public class MPCRestServiceException : Exception
    {
        public MPCRestServiceException()
        : base()
        {
        }

        public MPCRestServiceException(string message)
        : base(message)
        {
        }

        public MPCRestServiceException(string message, Exception e)
        : base(message, e)
        {
        }
    }
}
