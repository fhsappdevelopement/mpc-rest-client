﻿using PCM.MPC.WCFRest.Data;

namespace PCM.MPC.WCFRest.Serializer
{
    /// <summary>
    /// Representates the result of the deserializer. This is a special result
    /// adjusted for the PCM webservice. 
    /// </summary>
    /// <remarks>If no error is occured, the result is
    /// in the Response property, otherwise the result is null and the 
    /// response object is set. The response 
    /// This naming strategy is not intuitive but it's the way of the API.</remarks>
    /// <typeparam name="T"></typeparam>
    public class MPCDeserializerResult<T> where T : class
    {
        /// <summary>
        /// Result of the serializer.
        /// </summary>
        protected T result;

        /// <summary>
        /// Response.
        /// </summary>
        protected Response response;

        /// <summary>
        /// Result property.
        /// </summary>
        public T Result {
            get
            {
                return this.result;
            }
        }

        /// <summary>
        /// Response property.
        /// </summary>
        public Response Response {
            get
            {
                return this.response;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="response">Response object</param>
        public MPCDeserializerResult(Response response) : this(null, response)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="result">Result.</param>
        public MPCDeserializerResult(T result) : this(result, null)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="result"></param>
        /// <param name="response"></param>
        public MPCDeserializerResult(T result, Response response) {
            this.result = result;
            this.response = response;
        }

        /// <summary>
        /// Determines if a result is available.
        /// </summary>
        /// <returns>true if it is a result.</returns>
        public bool IsResponse()
        {
            return result == null && response != null;
        }
    }
}
