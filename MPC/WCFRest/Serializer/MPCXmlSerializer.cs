﻿using PCM.MPC.WCFRest.Data;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace PCM.MPC.WCFRest.Serializer
{
    public class MPCXmlSerializer
    {
        public MPCDeserializerResult<T> deserialize<T>(string xml) where T : class
        {
            MPCDeserializerResult<T> result;
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            // check if the response is a Response in xml ONLY IF T IS NOT A RESPONSE!
            if(typeof(T) != typeof(Response) && doc.DocumentElement.Name.Equals("Response"))
            {
                result = new MPCDeserializerResult<T>(this._deserialize<Response>(xml));
            } else
            {
                result = new MPCDeserializerResult<T>(this._deserialize<T>(xml));
            }

            return result;
        }

        public string serialize<T>(T toSerialize) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            using(StringWriter stringWriter = new StringWriter())
            using(XmlWriter xmlWriter = XmlWriter.Create(stringWriter))
            {
                serializer.Serialize(xmlWriter, toSerialize);

                return stringWriter.ToString();
            }
        }

        protected T _deserialize<T>(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            T result;

            using(TextReader reader = new StringReader(xml))
            {
                result = (T)serializer.Deserialize(reader);
            }

            return result;
        }
    }
}
