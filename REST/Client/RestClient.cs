﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PCM.REST.Client
{
    /// <summary>
    /// A simple client which use the .NET core System.Net.Http.HttpClient.
    /// Not configured for HTTPS! You can use the RestPath class to 
    /// define Paths with parameters, e.g. /foo/{0}/bar
    /// </summary>
    /// <remarks>This class also supports the TimeoutInMilliseconds Feature from
    /// the underlying HttpClient. Default shauld be 40 seconds.</remarks>
    public class RestClient : IDisposable
    {
        /// <summary>
        /// Store for the ServiceEndpoint property.
        /// </summary>
        protected Uri serviceEndpoint;

        /// <summary>
        /// ServiceEndpoint Property. E.g. http://example.com/myservice/
        /// </summary>
        /// <remarks>This property should be always an Uri to a valid REST-Webservice.
        /// This Endpoint will be prepended to the REST-Paths. REST-paths
        /// are the specific endpoints of a resource.</remarks>
        public Uri ServiceEndpoint
        {
            get
            {
                return this.serviceEndpoint;
            }
        }

        /// <summary>
        /// Store for the TimeoutInMilliseconds property.
        /// </summary>
        protected int timeoutInMilliseconds;

        /// <summary>
        /// TimeoutInMilliseconds property.
        /// </summary>
        /// <value>Time in milliseconds for the http-timeout.</value>
        public int TimeoutInMilliseconds
        {
            get
            {
                return this.httpClient.Timeout.Milliseconds;
            }
            set
            {
                this.httpClient.Timeout = TimeSpan.FromMilliseconds(value);
            }
        }

        /// <summary>
        /// Store for the .NET Core Http-client
        /// </summary>
        protected HttpClient httpClient;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint as Uri instance of the rest webservice which should be used.</param>
        public RestClient(Uri serviceEndpoint) : this(serviceEndpoint, new HttpClient())
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint as string of the rest webservice which should be used.</param>
        public RestClient(string serviceEndpoint) : this(new Uri(serviceEndpoint))
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint as string of the rest webservice which should be used.</param>
        /// <param name="httpClient">The .NET Core http client instance.</param>
        /// <remarks>You can use this constructor if you want to use a already configured http client instance.</remarks>
        public RestClient(string serviceEndpoint, HttpClient httpClient) : this(new Uri(serviceEndpoint), httpClient)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint as string of the rest webservice which should be used.</param>
        /// <param name="httpClient">The .NET Core http client instance.</param>
        /// <remarks>You can use this constructor if you want to use a already configured http client instance.</remarks>
        public RestClient(Uri serviceEndpoint, HttpClient httpClient)
        {
            this.serviceEndpoint = serviceEndpoint;
            this.httpClient = httpClient;
        }

        /// <summary>
        /// Calls the Dispose method on the underlying http client instance.
        /// </summary>
        public void Dispose()
        {
            this.httpClient.Dispose();
        }

        /// <summary>
        /// Http GET Request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Get(string restPath)
        {
            return await this.Get(new RestPath(restPath));
        }

        /// <summary>
        /// Http GET Request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="parameters">Array of string parameters which will be replaced in restPath, {i} will be replaced through parameters[i].</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Get(string restPath, string[] parameters)
        {
            return await this.Get(new RestPath(restPath, parameters));
        }

        /// <summary>
        /// Http GET Request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Get(Uri restPath)
        {
            return await this.Get(new RestPath(restPath));
        }

        /// <summary>
        /// Http GET Request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        /// <seealso cref="RestPath"/>
        public async Task<HttpResponseMessage> Get(RestPath restPath)
        {
            return await this.httpClient.GetAsync(restPath.ToUri(this.serviceEndpoint));
        }

        /// <summary>
        /// Http GET Request as stream (for downloading files).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>Stream</returns>
        /// <see cref="Stream"/>
        public async Task<Stream> GetAsStream(string restPath)
        {
            return await this.GetAsStream(new RestPath(restPath));
        }

        /// <summary>
        /// Http GET Request as stream (for downloading files).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="parameters">Array of string parameters which will be replaced in restPath, {i} will be replaced through parameters[i].</param>
        /// <returns>Stream</returns>
        /// <see cref="Stream"/>
        public async Task<Stream> GetAsStream(string restPath, string[] parameters)
        {
            return await this.GetAsStream(new RestPath(restPath, parameters));
        }

        /// <summary>
        /// Http GET Request as stream (for downloading files).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="param">Shorthand for only one parameter.</param>
        /// <returns>Stream</returns>
        /// <see cref="Stream"/>
        public async Task<Stream> GetAsStream(string restPath, string param)
        {
            return await this.GetAsStream(new RestPath(restPath, new[] { param }));
        }

        /// <summary>
        /// Http GET Request as stream (for downloading files).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>Stream</returns>
        /// <see cref="Stream"/>
        public async Task<Stream> GetAsStream(Uri restPath)
        {
            return await this.GetAsStream(new RestPath(restPath));
        }

        /// <summary>
        /// Http GET Request as stream (for downloading files).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <returns>Stream</returns>
        /// <see cref="Stream"/>
        public async Task<Stream> GetAsStream(RestPath restPath)
        {
            return await this.httpClient.GetStreamAsync(restPath.ToUri(this.serviceEndpoint));
        }

        /// <summary>
        /// Http POST request, configured for XML (MIME will be application/xml and encoding UTF8).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="parameters">Array of string parameters which will be replaced in restPath, {i} will be replaced through parameters[i].</param>
        /// <param name="xmlContent">XML body.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> PostXml(string restPath, string[] parameters, string xmlContent)
        {
            return await this.PostXml(new RestPath(restPath, parameters), xmlContent);
        }

        /// <summary>
        /// Http POST request, configured for XML (MIME will be application/xml and encoding UTF8).
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="xmlContent">XML body.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> PostXml(RestPath restPath, string xmlContent)
        {
            return await this.Post(restPath, xmlContent, Encoding.UTF8, "application/xml");
        }

        /// <summary>
        /// Http POST request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="content">Post body.</param>
        /// <param name="encoding">Enconding (prefer UTF8 if possible)</param>
        /// <param name="mimeType">Mimetype</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Post(RestPath restPath, string content, Encoding encoding, string mimeType)
        {
            return await this.Post(restPath, new StringContent(content, encoding, mimeType));
        }

        /// <summary>
        /// Http POST request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="content">Post Body.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Post(string restPath, HttpContent content)
        {
            return await this.Post(new RestPath(restPath), content);
        }

        /// <summary>
        /// Http POST request.
        /// </summary>
        /// <param name="restPath">Endpoint to a REST resource.</param>
        /// <param name="content">POST Body.</param>
        /// <returns>HttpResponseMessage</returns>
        /// <seealso cref="HttpResponseMessage">
        public async Task<HttpResponseMessage> Post(RestPath restPath, HttpContent content)
        {
            return await this.httpClient.PostAsync(restPath.ToUri(this.serviceEndpoint), content);
        }
    }
}
