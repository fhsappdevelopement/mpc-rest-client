﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PCM.REST.Client
{
    /// <summary>
    /// Describes a Path of a RESTful Service e.g. /foo/{i}/bar where i is a number
    /// of a parameter. 
    /// </summary>
    /// <remarks>To convert a RestPath object to an URI object it's neccessary
    /// to call the ToUri method with a service endpoint as parameter.</remarks>
    public class RestPath
    {
        /// <summary>
        /// Store for the uri string with placeholders like {i}.
        /// </summary>
        protected string formatUri;

        /// <summary>
        /// Store for the parameters which would be inserted in the formatUri.
        /// </summary>
        protected List<string> parameters;

        /// <summary>
        /// The Parameters property.
        /// </summary>
        public List<String> Parameters
        {
            get
            {
                return this.parameters;
            }
        }

        /// <summary>
        /// FormattedUriString property, readonly.
        /// </summary>
        /// <remarks>Returns the formatted Uri (who are the placeholders replaced through parameters).</remarks>
        public string FormattedUriString
        {
            get
            {
                return this.format(this.formatUri, this.parameters.ToArray());
            }
        }

        /// <summary>
        /// Converts the FormattedUriString into an URI instance.
        /// </summary>
        /// <param name="prependUri">The Endpoint of the REST Webservice.</param>
        /// <returns>Complete uri instance.</returns>
        /// <seealso cref="Uri"/>
        public Uri ToUri(Uri prependUri)
        {
            string formattedUri = this.FormattedUriString.TrimStart('/');
            string prependUriStr = prependUri.ToString();

            if (!prependUriStr.EndsWith("/"))
            {
                prependUriStr += "/";
            }

            return new Uri(new Uri(prependUriStr), formattedUri);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="formatUri">Endpoint of a REST Webservice resource.</param>
        public RestPath(string formatUri)
        {
            this.formatUri = formatUri;
            this.parameters = new List<string>();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="formatUri">Endpoint of a REST Webservice resource.</param>
        /// <param name="parameters">Array of string parameters which will be replaced in formatUri: {i} will be replaced through parameters[i].</param>
        public RestPath(string formatUri, params string[] parameters) : this(formatUri)
        {
            this.addParameters(parameters);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="uri"><Endpoint of a REST Webservice resource./param>
        public RestPath(Uri uri) : this(uri.ToString())
        {
        }

        /// <summary>
        /// Adds a parameter to the parameters list.
        /// </summary>
        /// <param name="param">Parameter.</param>
        /// <returns>this, for method chaining.</returns>
        public RestPath addParameter(string param)
        {
            this.parameters.Add(param);

            return this;
        }

        /// <summary>
        /// Adds an array of parameters to the parameters list.
        /// </summary>
        /// <param name="parameters">Array of parameters.</param>
        public void addParameters(string[] parameters)
        {
            foreach (string parameter in parameters)
            {
                this.addParameter(parameter);
            }
        }

        /// <summary>
        /// Replaces a parameter.
        /// </summary>
        /// <param name="index">Index position.</param>
        /// <param name="newValue">Replace value.</param>
        public void setParameter(int index, string newValue)
        {
            if (this.parameters.ElementAtOrDefault(index) != null)
            {
                this.parameters.Insert(index, newValue);
            }
        }

        /// <summary>
        /// Removes a parameter.
        /// </summary>
        /// <param name="index">Index position.</param>
        public void removeParameter(int index)
        {
            this.parameters.RemoveAt(index);
        }

        /// <summary>
        /// Removes a parameter.
        /// </summary>
        /// <param name="parameter">Parameter which should be removed. Does nothing if not found.</param>
        public void removeParameter(string parameter)
        {
            this.parameters.Remove(parameter);
        }

        /// <summary>
        /// Prepend an uri to this RestPath.
        /// </summary>
        /// <param name="prependUri">Uri</param>
        public void prepend(Uri prependUri)
        {
            this.prepend(prependUri.ToString());
        }

        /// <summary>
        /// Prepend an uri to this RestPath.
        /// </summary>
        /// <param name="prependUriString">Uri as string.</param>
        public void prepend(string prependUriString)
        {

            if (!(prependUriString.EndsWith("/") && this.formatUri.StartsWith("/")))
            {
                prependUriString += "/";
            }

            this.formatUri = prependUriString + this.formatUri;
        }

        /// <summary>
        /// Append an uri to this RestPath.
        /// </summary>
        /// <param name="appendUri">Uri</param>
        public void append(Uri appendUri)
        {
            this.append(appendUri.ToString());
        }

        /// <summary>
        /// Append an uri to this RestPath.
        /// </summary>
        /// <param name="appendUriString">Uri as string.</param>
        public void append(string appendUriString)
        {
            if (!(appendUriString.StartsWith("/") && this.formatUri.EndsWith("/")))
            {
                appendUriString = "/" + appendUriString;
            }

            this.formatUri += appendUriString;
        }

        /// <summary>
        /// Format the formatUri. Replaces the placeholders through the parameters.
        /// </summary>
        /// <param name="formatUri">Formatted Uri string with placeholders.</param>
        /// <param name="parameters">Array of Parameters.</param>
        /// <returns>Formatted string.</returns>
        protected string format(string formatUri, string[] parameters)
        {
            return string.Format(formatUri, parameters);
        }
    }
}
